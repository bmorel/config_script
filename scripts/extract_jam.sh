#!/bin/sh

# script to extract music from jamendo's zip archives in a classic directory tree and update mpd's db
for i in *.zip
do
	artiste=`echo $i|cut -f1 -d-|sed -e 's/^ *//g' -e 's/ *$//'`
	album=`echo $i|cut -f2 -d-|sed -e 's/^ *//g' -e 's/ *$//'`
	mkdir -p -- "$artiste/$album"
	7z x "$i" -o"$artiste/$album"
	mv "$i" ".."
	find . -path "./$artiste/$album/*" -exec chmod -x '{}' \;
done

mpc update -q
