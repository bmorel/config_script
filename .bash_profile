if [ -d "$HOME/scripts" ] ; then
	PATH="$HOME/scripts:$PATH"
fi

export CC=clang
export CXX=clang++
export MAKEFLAGS="-j `grep -c processor /proc/cpuinfo`"

if [ -z "$DISPLAY" ] && [ `tty` == /dev/tty1 ]
then
	startx
fi
