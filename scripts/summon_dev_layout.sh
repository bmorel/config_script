#!/bin/sh
if [ -e /tmp/shell_target ]; then
	cd `cat /tmp/shell_target`
fi

delay=0.03s
i3-msg 'split h' 
sleep $delay
x-terminal-emulator &
x-terminal-emulator &
sleep $delay
i3-msg 'split v' 
x-terminal-emulator &
